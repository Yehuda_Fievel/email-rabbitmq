const mongoose = require('mongoose');


const Request = new mongoose.Schema({
    requestId: {
        type: String,
        required: true,
    },
    method: {
        type: String,
        required: true,
    },
    path: {
        type: String,
        required: true,
    },
    query: {
        type: Object,
    },
    body: {
        type: Object,
    },
    headers: {
        type: Object,
        required: true,
    },
}, { timestamps: true });


module.exports = exports = mongoose.model('Request', Request, 'requests');