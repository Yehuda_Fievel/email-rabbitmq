const mongoose = require('mongoose');

mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
}).catch(error => console.log(error));

mongoose.connection.on('connected', () => console.log('Mongo connection established successfully'));

mongoose.connection.on('error', err => console.log(err));