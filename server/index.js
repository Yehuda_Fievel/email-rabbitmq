require('dotenv').config({ path: `${__dirname}/.env` });
const express = require('express');
const app = express();
const helmet = require('helmet');
const addRequestId = require('express-request-id');
const cors = require('cors');
const bodyParser = require('body-parser');
const port = process.env.PORT;


app.use(helmet());
app.use(addRequestId());
app.use(cors());
app.use(bodyParser.json());

require('./db');

require('./services/amqp').init();

require('./routes')(app);

process.on('unhandledRejection', error => {
    console.log('unhandledRejection: ' + error.message);
    console.log(error);
});

process.on('uncaughtException', (err, origin) => {
    console.log(`Exception origin: ${origin}`);
    console.log(`Caught exception: ${err}`);
});


app.listen(port, () => console.log(`App is listening on port ${port}!`));