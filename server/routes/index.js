const middlweare = require('./middleware');

module.exports = function (app) {
    app.use(middlweare.addApiRequest);


    app.use('/users', require('./users'));


    // Handles 404 request if no matching route to avoid sending back default express error
    app.use((req, res, next) => {
        const error = new Error('You do not belong here');
        console.log(error);
        res.status(404).send(error.message);
    });

    // Error Handler
    app.use((err, req, res, next) => {
        console.log(err);
        res.status(err.statusCode || 500).json({ error: true, message: err.message });
    });
}