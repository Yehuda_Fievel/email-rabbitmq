const express = require('express');
const router = express.Router();
const userSchema = require('../schema/users');
const userCtrl = require('../controllers/users');
const middleware = require('./middleware');


router.post('/register', userSchema.register, userCtrl.register);

router.post('/login', userSchema.login, userCtrl.login);

router.post('/url', middleware.authenticateUser, userSchema.sendUrl, userCtrl.sendUrl);

router.get('/url', userCtrl.approveUrl);


module.exports = router;