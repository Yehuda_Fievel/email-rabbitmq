const passport = require('passport');
const { Strategy, ExtractJwt } = require('passport-jwt');
const User = require('../models/users');
const Request = require('../models/requests')
const ErrorHandler = require('../utils/errorHandler');


passport.use(new Strategy({ jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), secretOrKey: process.env.JWT_SECRET }, async (jwt_payload, done) => {
    try {
        const user = await User.findOne({ _id: jwt_payload.sub });
        if (user) {
            return done(null, user);
        }
        return done(null, false);
    } catch (e) {
        return done(e, false);
    }
}));


function authenticateUser(req, res, next) {
    passport.authenticate('jwt', { session: false })(req, res, next);
}


async function addApiRequest(req, res, next) {
    try {
        const request = { requestId: req.id, method: req.method, path: req.path, headers: req.headers };
        if (Object.keys(req.query).length) {
            request.query = req.query;
        }
        if (Object.keys(req.body).length) {
            const body = { ...req.body };
            if (req.body.password) body.password = '[REDACTED]';
            if (req.body.confirmPassword) body.confirmPassword = '[REDACTED]';
            request.body = body;
        }
        await Request.create(request)
        next();
    } catch (error) {
        next(error);
    }
}


function hasRole(...permittedRoles) {
    return (req, res, next) => {
        try {
            if (permittedRoles.includes(req.user.role)) {
                return next();
            }
            throw new ErrorHandler(403, 'user does not have proper permissions');
        } catch (error) {
            next(error)
        }
    }
}


module.exports = {
    authenticateUser,
    addApiRequest,
    hasRole,
}