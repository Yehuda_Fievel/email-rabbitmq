const User = require('../models/users');
const amqp = require('../services/amqp')
const helpers = require('../utils/helpers');
const ErrorHandler = require('../utils/errorHandler');


async function register(req, res, next) {
    try {
        let user = await User.create(req.body);
        res.status(201).json({ response: { role: user.role, token: helpers.createJWT(user) }, message: 'User created' });
    } catch (e) {
        next(e);
    }
}


async function login(req, res, next) {
    try {
        const user = await User.findOne({ email: req.body.email });
        if (!user) {
            throw new ErrorHandler(404, 'user not found');
        }
        const isVerify = user.verifyPasswordSync(req.body.password);
        if (!isVerify) {
            throw new ErrorHandler(401, 'cannot verify user');
        }
        res.status(200).json({ response: { role: user.role, token: helpers.createJWT(user) } });
    } catch (e) {
        next(e);
    }
}


async function sendUrl(req, res, next) {
    try {
        await amqp.sendToQueue(req.body.url);
        res.status(200).json({ message: 'Request sent' });
    } catch (e) {
        next(e);
    }
}


async function approveUrl(req, res, next) {
    try {
        await amqp.get();
        await amqp.removeFromQueue();
        res.status(200).json({ message: 'URL approved' });
    } catch (e) {
        next(e);
    }
}


module.exports = {
    register,
    login,
    sendUrl,
    approveUrl
}