const amqp = require('amqplib');
const sendgrid = require('./sendgrid');

let channel = null;
const queueUrl = 'urls';
const queueEmail = 'email';


/**
 * Inititalize RabbitMQ - called from index.js
 */
async function init() {
    try {
        const connection = await amqp.connect(process.env.AMQP_URI || '');
        channel = await connection.createChannel();
        console.log('Connected to RabbitMQ');

        const [ok] = await Promise.all([
            // queue to keep track of urls to approve
            channel.assertQueue(queueUrl, { durable: false }),
            // queue to track emails sent
            channel.assertQueue(queueEmail, { durable: false }),
        ])
        console.log(ok)
    } catch (error) {
        console.log(error)
    }
}


/**
 * Send url to queue
 * @param {string} msg - url to add to queue
 */
async function sendToQueue(msg) {
    await channel.sendToQueue(queueUrl, Buffer.from(msg));
    console.log('message sent to queue');
    const res = await channel.get(queueEmail);
    // if no pending emails to approve send new email
    if (!res.content) {
        await get().catch(e => console.log(e));
    } else {
        // add back to queue since email not approved
        channel.nack(res);
    }
}


/**
 * Get next url from queue
 */
async function get() {
    const msg = await channel.get(queueUrl, { noAck: true });
    if (msg && msg.content) {
        // on production site the approving user would have credentials
        // and add a one-time use code added in the querystring of of the approval link for auth
        const html = `<a href="${msg.content.toString()}">${msg.content.toString()}</a><br/><a href="${process.env.SERVER_URI}/users/url">Approve URL</a>`;
        console.log(html)
        await sendgrid.sendMail({ subject: 'Approve link', html });
        await channel.sendToQueue(queueEmail, Buffer.from([true]));
    }
}


/**
 * Remove item from queue after email is approved
 */
async function removeFromQueue() {
    await channel.get(queueEmail, { noAck: true });
}


module.exports = {
    init,
    sendToQueue,
    get,
    removeFromQueue,
}