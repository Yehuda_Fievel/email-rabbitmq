const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(process.env.SENDGRID_API_KEY);


/**
 * Send an email
 * @param {string} param0.to - email recipient
 * @param {string} param0.from - email sender
 * @param {string} param0.subject - email subject
 * @param {string} param0.html - html of email body
 * @param {string} param0.templateId - id of dynamic template
 * @param {string} param0.templateData - data of dynamic template
 */
async function sendMail({ to = process.env.FROM_EMAIL, from = process.env.FROM_EMAIL, subject, html, templateId, templateData }) {
    try {
        const msg = { to, from, subject, html, templateId, dynamic_template_data: templateData };
        await sgMail.send(msg);
        console.log(`email sent to ${to}`);
    } catch (e) {
        throw e;
    }
}


module.exports = {
    sendMail,
}