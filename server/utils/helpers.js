const jwt = require('jsonwebtoken');


/**
 * Safely JSON parse string
 * @param {string} data - string to parse 
 */
function safeParseJson(data) {
    try {
        return JSON.parse(data)
    } catch (error) {
        return data
    }
}

/**
 * Create JWT token
 * @param {object} user
 * @returns - jwt token
 */
function createJWT(user) {
    const sign = {
        iss: 'server',
        sub: user._id,
        iat: Date.now(),
    }
    return token = jwt.sign(sign, process.env.JWT_SECRET);
}


module.exports = {
    safeParseJson,
    createJWT,
}