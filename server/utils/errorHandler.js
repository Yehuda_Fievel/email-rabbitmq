/**
 * ErrorHandler class to add statusCode to Error class
 */
class ErrorHandler extends Error {
    constructor(statusCode, message) {
        super();
        this.statusCode = statusCode;
        this.message = message;
    }
}

module.exports = ErrorHandler