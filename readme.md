## For Server

- Create an .env file from .env.sample
- Type ` cd server/`

### Run as standalone

- Make sure to have MongoDB running
- For those who don't have RabbitMQ installed run, `docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management`
- Run `npm i`
- Run `npm start`

### Run with Docker

- Run `docker-compose up`

## For Front

- Type `cd front/`
- Run `npm i`
- Run `npm start`
