import React from 'react';


const ManagementContext = React.createContext({
  role: localStorage.getItem('role') || null,
  setRole: () => {},
  isAuthenticated: localStorage.getItem('token') || false,
  setIsAuthenticated: () => {},
  alert: null,
  setAlert: () => {},
})

export default ManagementContext