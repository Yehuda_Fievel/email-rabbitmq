import React from 'react'
import { Form, Input, Button } from 'antd'
import useFetchData from '../hooks/useFetchData'


const SendUrl = () => {
    const { loading, sendRequest } = useFetchData()

    const layout = {
        labelCol: { span: 3 },
        wrapperCol: { span: 16 },
    }
    const tailLayout = {
        wrapperCol: { offset: 8, span: 16 },
    }

    const onFinish = values => {
        sendRequest('POST', 'users/url', values)
    }

    return (
        <Form
            {...layout}
            name='register'
            onFinish={values => onFinish(values)}
        >
            <Form.Item
                label='Url'
                name='url'
                rules={[ { required: true, message: 'Please enter the url' }]}
            >
                <Input />
            </Form.Item>                     
            <Form.Item {...tailLayout}>
                <Button type='primary' loading={loading} htmlType='submit'>Submit</Button>
            </Form.Item>
        </Form>   
	)
}


export default SendUrl