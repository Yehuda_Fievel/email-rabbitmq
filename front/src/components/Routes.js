import React, { useContext } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import ManagementContext from '../context/Management'
import Home from './Home'
import SendUrl from './SendUrl'


const ProtectedRoute = ({ component: Component, isAuthenticated, path }) => (
    <Route 
        path={path}
        render={props => isAuthenticated ? <Component {...props} /> : <Redirect to='/' />}
    />
)

const Routes = () => {
    const { isAuthenticated } = useContext(ManagementContext)

    return (
        <Switch>
            <Route exact path='/' component={Home} />
            <ProtectedRoute isAuthenticated={isAuthenticated} path='/sendUrl' component={SendUrl} />
        </Switch>
    )
}

export default Routes